import Siema from "siema"

window.addEventListener("load", () => {
  const carousels = document.querySelectorAll("figure.carousel") as NodeListOf<
    HTMLElement
  >
  carousels.forEach(carousel => {
    const siema = new Siema({
      selector: carousel
    })
    const cursor = document.createElement("div")
    cursor.classList.add("cursor")
    carousel.appendChild(cursor)
    carousel.addEventListener("mouseleave", e => {
      cursor.style.visibility = "hidden"
    })
    carousel.addEventListener("mouseover", e => {
      cursor.style.visibility = "visible"
    })
    carousel.addEventListener("mousemove", e => {
      const m = e as MouseEvent
      cursor.style.left = m.pageX - cursor.clientWidth / 2 + "px"
      cursor.style.top = m.pageY - cursor.clientHeight / 2 + "px"
      const rect = carousel.getBoundingClientRect()
      const halfX = rect.left + (rect.right - rect.left) / 2
      if (m.pageX < halfX) {
        cursor.classList.remove("next")
        cursor.classList.add("prev")
      } else {
        cursor.classList.remove("prev")
        cursor.classList.add("next")
      }
    })
    let downX = 0
    let downY = 0
    carousel.addEventListener("mousedown", e => {
      downX = e.pageX
      downY = e.pageY
    })
    carousel.addEventListener("mouseup", e => {
      if (e.pageX === downX && e.pageY === downY) {
        if (cursor.classList.contains("prev")) {
          siema.prev()
        } else {
          siema.next()
        }
      }
    })
  })
})